class CreateDishes < ActiveRecord::Migration
  def change

    create_table :dishes do |t|
      t.string :dishname
      t.integer :flag
      t.string :alvalue
      t.string :image
      t.text :recipe
      t.text :food
      t.references :genre, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
