class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.string :fname
      t.integer :calorie

      t.timestamps null: false
    end
  end
end
