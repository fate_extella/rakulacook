Rails.application.routes.draw do
  ## 提案する献立の表示＆レシピの表示  2016/10/03追加　井上
  get 'dish' => 'recipe_pages#result'
  get 'recipe/:id' => 'recipe_pages#recipe'
  get 'dish/0' => 'recipe_pages#sorry'
  get 'fav' => 'recipe_pages#fav'
  ## end

  get 'recipe/:id/fav' => 'recipe_pages#fav'

  get 'manu_disp/menu_disp'
  get 'question_result/question_result'

  post 'menu/question_result' => 'manu_pages#question_result'
  post 'question'=>'manu_pages#question'

  get 'init'=>'manu_pages#init'

  get 'question'=>'manu_pages#question'

  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'signup' => 'user_pages#regist'

  get 'user_pages/:id/allergies' => 'user_pages#allergies'

  post 'user_pages/:id/allergies/allergiesinput' => 'user_pages#allergiesinput'

  get 'user_pages/:id/favdeleteselect' => 'user_pages#favdeleteselect'

  post 'user_pages/:id/favdeleteselect/favdelete' => 'user_pages#favdelete'

  get 'user_pages/:id/namechange' => 'user_pages#namechange'

  get 'user_pages/:id/newemailinput' => 'user_pages#newemailinput'

  post 'user_pages/:id/emailchange/sentnewmail' => 'user_pages#sentnewmail'

  get 'user_pages/:id/emailchange' => 'user_pages#emailchange', :as => :emailchange

  get 'drawalinput' => 'user_pages#destroyinput'

  post 'drawalconfirm' => 'user_pages#destroyconfirm'

  post 'signup/confirm' => 'user_pages#confirm'

  get 'about' =>'static_pages/about'

  root 'static_pages#home'

  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  resources :user_pages
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]

end
