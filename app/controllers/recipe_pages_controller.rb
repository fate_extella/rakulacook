class RecipePagesController < ApplicationController

  def result

    if session[:maindish].nil? or session[:subdish].nil? or session[:people].nil?
      redirect_to root_path
    else
      if session[:maindish] == 0
        redirect_to action: 'sorry'
      else
        @main = Dish.find(session[:maindish])
        if session[:subdish] != 0
          @sub = Dish.find(session[:subdish])
        else
          @sub = nil
        end
      end
    end
  end

  def recipe
    if session[:maindish].nil? or session[:subdish].nil? or session[:people].nil?
      redirect_to root_path
    else
      @recipe = JSON.parse(Dish.find(params[:id]).recipe)
      @food = JSON.parse(Dish.find(params[:id]).food)
      @favcnt = Dish.find(params[:id])
      if logged_in?
        @user = current_user
      end
    end
  end

  def fav
    @recipe = Dish.find(params[:id])
    @user = current_user
    @user.dishes << @recipe
#    @recipe.users << @user
  end


  def sorry
    if session[:maindish].nil? or session[:subdish].nil? or session[:people].nil?
      redirect_to root_path
    end
  end
end
