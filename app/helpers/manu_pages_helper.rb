module ManuPagesHelper
  def person_choices
    [["1人", 1],
     ["2人", 2],
     ["3人", 3],
     ["4人", 4],
     ["5人", 5],
     ["6人", 6],
     ["7人", 7],
     ["8人", 8],
     ["9人", 9],
     ["10人", 10]
    ]
  end

  def side_choices
    [["なし", 0],
     ["1品", 1],
     ["2品", 2],
     ["3品", 3],
     ["4品", 4],
     ["5品", 5]
    ]
  end

  def zyannru_choices
    [["和食", "japanese"],
    ["洋食", "western"],
    ["中華", "china"]
    ]
  end

  def time_choices
    [["ある", "yes"],
    ["ない", "no"]
    ]
  end

  def main_choices
    [["お肉", "meet"],
     ["お魚", "fish"],
     ["それ以外", "other"]
    ]

  end

end
